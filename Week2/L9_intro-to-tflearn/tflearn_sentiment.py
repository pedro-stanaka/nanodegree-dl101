import pandas as pd
import numpy as np
import tensorflow as tf
import tflearn
from tflearn.data_utils import to_categorical
from collections import Counter

reviews = pd.read_csv('reviews.txt', header=None)
labels = pd.read_csv('labels.txt', header=None)

total_counts = Counter()
for i, r in reviews.iterrows():
    total_counts.update(r.values[0].split(' '))

print("Total words in data set: ", len(total_counts))
vocab = sorted(total_counts, key=total_counts.get, reverse=True)[:10000]
print(vocab[:60])

print(vocab[-1], ': ', total_counts[vocab[-1]])

word2idx = {}

for i, w in enumerate(vocab):
    word2idx[w] = i


def text_to_vector(text):
    global word2idx
    global vocab

    word_count = Counter(text.split(' '))
    words = np.zeros(len(vocab))

    for word, c in word_count.most_common():
        if word in word2idx.keys():
            words[word2idx[word]] = c

    return words


# Network building
def build_model(input_size=10000, hidden_layers=1, hidden_units=10, learning_rate=0.089):
    # This resets all parameters and variables
    tf.reset_default_graph()

    # Create input layer
    nn = tflearn.input_data([None, input_size])
    # Add up to ``hidden_layers``
    for _ in range(hidden_layers):
        nn = tflearn.fully_connected(nn, n_units=hidden_units, activation='ReLU')

    # Create output layer
    nn = tflearn.fully_connected(nn, n_units=2, activation='softmax')

    # Specify the learning process parameters and functions
    nn = tflearn.regression(nn, optimizer='sgd', learning_rate=learning_rate, loss='categorical_crossentropy')

    model = tflearn.DNN(nn)
    return model
