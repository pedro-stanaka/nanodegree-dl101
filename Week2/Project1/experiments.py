import numpy as np

from Week2.Project1.neural_network import NeuralNetwork

inputs = np.array([[0.5, -0.2, 0.1]])
targets = np.array([[0.4]])
test_w_i_h = np.array([[0.1, -0.2],
                       [0.4, 0.5],
                       [-0.3, 0.2]])
test_w_h_o = np.array([[0.3],
                       [-0.1]])


if __name__ == '__main__':
    network = NeuralNetwork(3, 2, 1, 0.5)
    network.weights_input_to_hidden = test_w_i_h.copy()
    network.weights_hidden_to_output = test_w_h_o.copy()

    network.train(inputs, targets)

    print(network.weights_input_to_hidden)
    print(np.shape(inputs))
    print(np.shape(test_w_h_o))
    print(inputs * test_w_h_o)
