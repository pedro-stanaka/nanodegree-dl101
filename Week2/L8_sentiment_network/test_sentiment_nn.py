from sentiment_network import SentimentNetwork

g = open('reviews.txt', 'r')  # What we know!
reviews = list(map(lambda x: x[:-1], g.readlines()))
g.close()

g = open('labels.txt', 'r')  # What we WANT to know!
labels = list(map(lambda x: x[:-1].upper(), g.readlines()))
g.close()

mlp = SentimentNetwork(reviews[:-1000], labels[:-1000], learning_rate=0.01)

mlp.train(reviews[:-1000], labels[:-1000])

mlp.test(reviews[-100:], labels[-100:])
