import sys
import time
from collections import Counter

import numpy as np


# Encapsulate our neural network in a class
class SentimentNetwork:
    def __init__(self, reviews, labels, hidden_nodes=10, learning_rate=0.1, min_count=50, polarity_cutoff=1.5):
        """Create a SentimenNetwork with the given settings
        Args:
            reviews(list) - List of reviews used for training
            labels(list) - List of POSITIVE/NEGATIVE labels associated with the given reviews
            hidden_nodes(int) - Number of nodes to create in the hidden layer
            learning_rate(float) - Learning rate to use while training

        """
        # Assign a seed to our random number generator to ensure we get
        # reproducible results during development
        np.random.seed(1)

        # process the reviews and their associated labels so that everything
        # is ready for training
        self.pre_process_data(reviews, labels, min_count, polarity_cutoff)

        # Build the network to have the number of hidden nodes and the learning rate that
        # were passed into this initializer. Make the same number of input nodes as
        # there are vocabulary words and create a single output node.
        self.init_network(len(self.review_vocab), hidden_nodes, 1, learning_rate)

    def pre_process_data(self, reviews, labels, min_count, polarity_cutoff):

        wd_count = Counter()

        positive_count = Counter()
        negative_count = Counter()

        # Count the words and also separate them in POSITIVE and NEGATIVE
        for r, l in zip(reviews, labels):
            rwords = r.split(' ')
            wd_count.update(rwords)
            if l == 'POSITIVE':
                positive_count.update(rwords)
            else:
                negative_count.update(rwords)

        # Calculate the positive to negative ratio and also remove
        # least used words
        pos_neg_ratios = Counter()
        for w, count in wd_count.most_common():
            if count >= 50:
                posneg_ratio = (negative_count[w] / float(1 + positive_count[w]))
                pos_neg_ratios[w] = np.log(posneg_ratio) if posneg_ratio > 1 else -np.log((1/(posneg_ratio + 0.01)))

        # populate review_vocab with all of the words in the given reviews
        # that match the min_count and polarity_cutoff threshold
        review_vocab = set()
        for w, count in wd_count.most_common():
            if count > min_count:
                if w in pos_neg_ratios.keys():
                    if (pos_neg_ratios[w] >= polarity_cutoff) or (pos_neg_ratios[w] <= -polarity_cutoff):
                        review_vocab.add(w)
                else:
                    review_vocab.add(w)

        # Convert the vocabulary set to a list so we can access words via indices
        self.review_vocab = list(review_vocab)

        # populate label_vocab with all of the words in the given labels.
        label_vocab = set(labels)

        # Convert the label vocabulary set to a list so we can access labels via indices
        self.label_vocab = list(label_vocab)

        # Store the sizes of the review and label vocabularies.
        self.review_vocab_size = len(self.review_vocab)
        print('Resulting vocab size: %d' % self.review_vocab_size)
        self.label_vocab_size = len(self.label_vocab)

        # Create a dictionary of words in the vocabulary mapped to index positions
        self.word2index = {}
        # populate self.word2index with indices for all the words in self.review_vocab
        for i, word in enumerate(self.review_vocab):
            self.word2index[word] = i

        # Create a dictionary of labels mapped to index positions
        self.label2index = {}
        for i, label in enumerate(self.label_vocab):
            self.label2index[label] = i

    def init_network(self, input_nodes, hidden_nodes, output_nodes, learning_rate):
        # Store the number of nodes in input, hidden, and output layers.
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = output_nodes

        # Store the learning rate
        self.learning_rate = learning_rate

        # Initialize weights

        #       the input layer and the hidden layer.
        self.weights_0_1 = np.zeros((self.input_nodes, self.hidden_nodes))

        #       These are the weights between the hidden layer and the output layer.
        self.weights_1_2 = np.random.normal(loc=0.1, scale=self.output_nodes ** -0.5,
                                            size=(self.hidden_nodes, self.output_nodes))

        self.layer_1 = np.random.normal(loc=0.1, size=(1, self.hidden_nodes))

    def get_target_for_label(self, label):
        return 0 if label == 'NEGATIVE' else 1

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def sigmoid_output_2_derivative(self, output):
        return output * (1 - output)

    def train(self, training_reviews_raw, training_labels):

        # make sure out we have a matching number of reviews and labels
        assert (len(training_reviews_raw) == len(training_labels))

        training_reviews = list()
        for r in training_reviews_raw:
            review_index = set()
            for w in r.split(' '):
                if w in self.word2index:
                    review_index.add(self.word2index[w])
            training_reviews.append(list(review_index))

        # Keep track of correct predictions to display accuracy during training
        correct_so_far = 0

        # Remember when we started for printing time statistics
        start = time.time()

        input_size = len(training_labels)

        # loop through all the given reviews and run a forward and backward pass,
        # updating weights for every item
        for i in range(len(training_reviews)):

            # Get the next review and its correct label
            review = training_reviews[i]
            label = training_labels[i]

            # Implement the forward pass through the network.
            #       That means use the given review to update the input layer,
            #       then calculate values for the hidden layer,
            #       and finally calculate the output layer.
            #
            #       Do not use an activation function for the hidden layer,
            #       but use the sigmoid activation function for the output layer.

            # Now we have an matrix of (1, hidden_nodes)
            self.layer_1 *= 0
            for idx in review:
                self.layer_1 += self.weights_0_1[idx]

            layer_1_output = np.dot(self.layer_1, self.weights_1_2)

            nn_prediction = self.sigmoid(layer_1_output)

            # TODO: Implement the back propagation pass here.
            #       That means calculate the error for the forward pass's prediction
            #       and update the weights in the network according to their
            #       contributions toward the error, as calculated via the
            #       gradient descent and back propagation algorithms you
            #       learned in class.

            error = nn_prediction - self.get_target_for_label(label)

            layer_2_delta = error * self.sigmoid_output_2_derivative(nn_prediction)

            layer_1_error = np.dot(layer_2_delta, self.weights_1_2.T)
            layer_1_delta = layer_1_error

            self.weights_1_2 -= (np.dot(self.layer_1, self.weights_1_2) * self.learning_rate) / input_size

            for idx in review:
                self.weights_0_1[idx] -= layer_1_delta[0] * self.learning_rate

            # Keep track of correct predictions. To determine if the prediction was
            #       correct, check that the absolute value of the output error
            #       is less than 0.5. If so, add one to the correct_so_far count.

            if error < 0.5:
                correct_so_far += 1

            # For debug purposes, print out our prediction accuracy and speed
            # throughout the training process.

            elapsed_time = float(time.time() - start)
            reviews_per_second = i / elapsed_time if elapsed_time > 0 else 0

            sys.stdout.write("\rProgress:" + str(100 * i / float(len(training_reviews)))[:4] \
                             + "% Speed(reviews/sec):" + str(reviews_per_second)[0:5] \
                             + " #Correct:" + str(correct_so_far) + " #Trained:" + str(i + 1) \
                             + " Training Accuracy:" + str(correct_so_far * 100 / float(i + 1))[:4] + "%")
            if i % 2500 == 0:
                print("")

    def test(self, testing_reviews, testing_labels):
        """
        Attempts to predict the labels for the given testing_reviews,
        and uses the test_labels to calculate the accuracy of those predictions.
        """

        # keep track of how many correct predictions we make
        correct = 0

        # we'll time how many predictions per second we make
        start = time.time()

        # Loop through each of the given reviews and call run to predict
        # its label.
        for i in range(len(testing_reviews)):
            pred = self.run(testing_reviews[i])
            if pred == testing_labels[i]:
                correct += 1

            # For debug purposes, print out our prediction accuracy and speed
            # throughout the prediction process.

            elapsed_time = float(time.time() - start)
            reviews_per_second = i / elapsed_time if elapsed_time > 0 else 0

            sys.stdout.write("\rProgress:" + str(100 * i / float(len(testing_reviews)))[:4] \
                             + "% Speed(reviews/sec):" + str(reviews_per_second)[0:5] \
                             + " #Correct:" + str(correct) + " #Tested:" + str(i + 1) \
                             + " Testing Accuracy:" + str(correct * 100 / float(i + 1))[:4] + "%")

    def run(self, review_raw):
        """
        Returns a POSITIVE or NEGATIVE prediction for the given review.
        """
        self.layer_1 *= 0

        review = set()
        for w in review_raw.lower().split(' '):
            if w in self.word2index.keys():
                review.add(self.word2index[w])
        review = list(review)

        for idx in review:
            self.layer_1 += self.weights_0_1[idx]

        # The output here is a 1x1 matrix, so we take only the scalar
        layer_2 = self.sigmoid(np.dot(self.layer_1, self.weights_1_2))[0]

        # TODO: The output layer should now contain a prediction.
        #       Return `POSITIVE` for predictions greater-than-or-equal-to `0.5`,
        #       and `NEGATIVE` otherwise.
        return 'POSITIVE' if layer_2 >= 0.5 else 'NEGATIVE'


if __name__ == '__main__':
    g = open('reviews.txt', 'r')  # What we know!
    reviews = list(map(lambda x: x[:-1], g.readlines()))
    g.close()

    g = open('labels.txt', 'r')  # What we WANT to know!
    labels = list(map(lambda x: x[:-1].upper(), g.readlines()))
    g.close()

    mlp = SentimentNetwork(reviews[:-1000], labels[:-1000], min_count=20, polarity_cutoff=0.8, learning_rate=0.01)
    mlp.train(reviews[:-1000], labels[:-1000])
    print("\n\n#### Testing...")
    mlp.test(reviews[-1000:], labels[-1000:])
