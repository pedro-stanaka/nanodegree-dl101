import numpy as np

label_to_idx = {
    0: 'airplane',
    1: 'automobile',
    2: 'bird',
    3: 'cat',
    4: 'deer',
    5: 'dog',
    6: 'frog',
    7: 'horse',
    8: 'ship',
    9: 'truck'
}


def one_hot_encode(x):
    """
    One hot encode a list of sample labels. Return a one-hot encoded vector for each label.
    : x: List of sample Labels
    : return: Numpy array of one-hot encoded labels
    """
    encodes = np.array((len(x), 10))
    for i, label in enumerate(x):
        encodes[i] = np.array(10)
        encodes[i][label] = 1.0

    return encodes


print(one_hot_encode([1, 1, 5]))

