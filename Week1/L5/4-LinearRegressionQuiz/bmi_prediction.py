import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

# Load the dataset from the CSV file
bmi_life_data = pd.read_csv('dataset.csv')
bmi = bmi_life_data[['BMI']]
life_expectancy = bmi_life_data[['Life expectancy']]

# Plot the data from the file
plt.scatter(bmi, life_expectancy)

# Find the regression model
bmi_model = LinearRegression()
bmi_model.fit(bmi, life_expectancy)

# predict a value using the model we built
print(bmi_model.predict(21.07391))

# Plot the final result with the linear model
plt.plot(bmi, bmi_model.predict(bmi))
plt.show()
